/*
Session 30 Express.js - Data Persistence via Mongoose ODM

ODM
	object document mapper
	translates objects in code to documents for use in document-based databases such as MongoDB
	for NoSQL

ODM library
	manages data relationships 

Schema
	reprsentation of a document's structure. It also contains a document's expected properties and data types 
	blueprint

Models
	programming interface for querying or manipulating a database 
	has methods that simplify such operations




1. npm init -y (initialize node package manager)
2. npm install express mongoose (installing express and mongoose)
3. touch gitignore (ignore node_modules)


Mini Activity 
	create an expressjs API designated to port 4000
	create a new route with endpoint /hello and method GET 
		-should be able to respond with "Hello World"

1. require directives: express, mongoose ...
2. putting directives methods in variables 
3. put port in var 
4. mongoose.connect('link from mongoDB', 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	})

5. APIs

	.use method for json files 
		setup for allowing the server to handle json files (data from requests)

	.use method for urlencoded
		allows our app to read data from forms

6. routes 
*/


const express = require('express');
const mongoose = require('mongoose');
//Mongoose is a package that allows creation of Schemas to model our data
//Also has access to a number of methods for manipulating our database


const app = express();
const port = 3001;

mongoose.connect('mongodb+srv://admin:admin123@course-booking.fyojt.mongodb.net/B157_to-do?retryWrites=true&w=majority',
{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

//set notifications for connection success or failure\
//Connection to the database
//not required. this is just for confirmation
//Allows us to handle the errors when the initial connection is established
let db = mongoose.connection;

//if a connection error occured, output in our console
//.bind para masama lang yung connection error string natin tsaka yung error talaga from console 
db.on('error', console.error.bind(console, 'Connection error'));

//if the connection is successful, output in our console
db.once('open', () => console.log(`We're connected to the cloud database`));

//Schemas determine the structure of the documents to be written in the database
//this acts as a blueprints to our data
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});


//.model uses schemas and are used to create/instantiate objects that correspond to the schema 
//Models (the variable) must be in a singular form and capitalized
//The first parameter of the mongoose model method indicates the collection in where will be stored in the MongoDB collection 
//The second parameter is used to specify the schema/the blueprint of the documents
//Using mongoose, the package was programmed well enough that it automatically converts teh singular form of the model name into prlural form when creating a collection 
const Task = mongoose.model('Task', taskSchema)

//Task >>>> tasks (to database) 

app.use(express.json());

app.use(express.urlencoded({extended:true}))


//Creating a new Task 
/*
	1. Add a functionality to check if there are duplicate tasks 
		-if the task already exists in the database, we return an error 
		- if the task tasks doesnt exts, we add it in our database 
	2. The task data will be coming from the request body
	3. Create a Task object with a 'name' field/property

*/

app.post('/tasks', (req, res) => 
{
	Task.findOne({name: req.body.name}, (err, result) => {

		if (result != null && result.name == req.body.name){
			return res.send('Duplicate task found');
		} else {
			let newTask = new Task({
				name: req.body.name
			});


			//function parameters: (error, result)
			newTask.save((saveErr, savedTask) => {

				if(saveErr) {
					return console.error(saveErr)
				} else {
					return res.status(201).send('New Task created.')
				}
			})
		}
	})
})


//GET request to retrieve all the documents

/*
	1. retrieve all the documents
	2. if an error is encountered, print the error 
	3. if no errors are found, send a success back to the client  

*/

app.get('/tasks', (req,res) => {
	Task.find({}, (err, result) => {
		if(err) {
			console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})






})


/*ACTIVITY
	username: 
	password:
*/

// 1. Create a User schema.
const userSchema = new mongoose.Schema({
	username: String,
	password: String
})



// 2. Create a User model.
const User = mongoose.model('User', userSchema)

// 3. Create a POST route that will access the "/signup" route that will create a user.
// 4. Process a POST request at the "/signup" route using postman to register a user.
app.post('/signup', (req, res) => {
	User.findOne({username: req.body.username}, (err, result) => {
		if (result != null && result.username == req.body.username) {
			return res.send('Username already exist')
		} else if (req.body.username !=='' && req.body.password !== '') {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})

			newUser.save((saveErr, savedUser) => {
				if (saveErr) {
					return console.error(saveErr);
				} else {
					return res.status(201).send('New user registered.')
				}
			})
		} else {
			return res.status(404).send('Empty username/password')
		}
	})
})




// 5. Create a git repository named S30.
// 6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 7. Add the link in Boodle.


app.get('/hello', (req,res) => res.send('Hello World'));

app.listen(port, () => console.log(`Server is running at localhost: ${port}`));
